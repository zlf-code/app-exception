<?php
declare(strict_types=1);

namespace Zlf\AppException;
/**
 *
 * 系统错误状态码
 * Class ErrorCode
 * @package Core\Constants
 */
class ExceptionCode
{
    const SUCCESS = 200;//请求成功
    const MSG_FAIL = 400;//请求失败
    const NOT_FOUND = 404;//内容不存在
    const VALIDATION = 417;//数据验证错误异常代码
    const DATA_ERROR = 419;//数据结构错误时抛出的异常
    const OPTION = 501;//其他错误
    const ERROR = 500;//系统错误
    const NOT_LOGGED_IN = 403;//未登录
    const EXPIRE_LOGGED_IN = 402;//登录已过期
    const NOT_AUTHORITY = 401;//无访问权限
}