<?php
declare(strict_types=1);

namespace Zlf\AppException\Exception;

use Zlf\AppException\ExceptionCode;

class AuthorizeException extends BasicsException
{
    /**
     * NotFoundException constructor.
     * @param string $message
     * @param null $raw
     */
    public function __construct(string $message = "没有访问权限", $raw = null)
    {
        parent::__construct($message, ExceptionCode::NOT_AUTHORITY, $raw);
    }
}