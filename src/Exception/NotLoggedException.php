<?php
declare(strict_types=1);

namespace Zlf\AppException\Exception;

use Zlf\AppException\ExceptionCode;

class NotLoggedException extends BasicsException
{
    /**
     * NotFoundException constructor.
     * @param string $message
     * @param null $raw
     */
    public function __construct(string $message = "请先登录", $raw = null)
    {
        parent::__construct($message, ExceptionCode::NOT_LOGGED_IN, $raw);
    }
}