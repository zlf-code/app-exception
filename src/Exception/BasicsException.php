<?php

declare(strict_types=1);

namespace Zlf\AppException\Exception;

use Exception;
use Throwable;

class BasicsException extends Exception
{

    private mixed $_raw;

    /**
     * Exception constructor.
     * @param string $message
     * @param int $code
     * @param null $raw
     * @param Throwable|null $previous
     */
    public function __construct(string $message = "", int $code = 0, mixed $raw = null, Throwable $previous = null)
    {
        if (!is_null($raw)) {
            $this->setRaw($raw);
        }
        parent::__construct($message, $code, $previous);
    }


    /**
     * 设置异常元数据
     * @param $raw
     * @author 竹林风@875384189 2021/3/21 11:21
     */
    private function setRaw($raw): void
    {
        $this->_raw = ['data' => $raw];
    }

    /**
     * 获取异常元数据
     * @return mixed
     * @author 竹林风@875384189 2021/3/21 11:21
     */
    public function getRaw(): mixed
    {
        return $this->_raw;
    }


    /**
     * 转数组
     * @return array
     */
    public function toArray(): array
    {
        return [
            'code' => $this->getCode(),
            'file' => $this->getFile(),
            'line' => $this->getLine(),
            'info' => $this->getMessage(),
            'time' => time(),
            'data' => $this->getRaw(),
        ];
    }


    /**
     * 转字符串
     * @return string
     */
    public function toString(): string
    {
        return $this->getMessage();
    }
}