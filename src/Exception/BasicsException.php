<?php

declare(strict_types=1);

namespace Zlf\AppException\Exception;

use Exception;
use Throwable;

/**
 * 基础异常类
 * 提供了额外的功能，如设置和获取元数据，以及将异常信息转换为数组或字符串。
 */
class BasicsException extends Exception
{

    /**
     * 异常的原始数据或其他元数据
     * @var mixed
     */
    private $_raw;


    /**
     * 构造函数
     * 初始化一个新的异常实例。
     *
     * @param string $message 异常消息，默认为空字符串
     * @param int $code 异常代码，默认为0
     * @param mixed|null $raw 异常元数据，默认为null
     * @param Throwable|null $previous 前一个异常，默认为null
     */
    public function __construct($message = "", $code = 0, $raw = null, Throwable $previous = null)
    {
        if (!is_null($raw)) {
            $this->setRaw($raw);
        }
        parent::__construct($message, $code, $previous);
    }


    /**
     * 设置异常元数据
     *
     * @param mixed $raw 元数据
     * @return void
     */
    private function setRaw($raw)
    {
        $this->_raw = ['data' => $raw];
    }

    /**
     * 获取异常元数据
     *
     * @return mixed 返回存储的元数据
     */
    public function getRaw()
    {
        return $this->_raw;
    }


    /**
     * 将异常信息转换为数组
     *
     * @return array 包含异常详细信息的数组
     */
    public function toArray()
    {
        return [
            'code' => $this->getCode(),
            'file' => $this->getFile(),
            'line' => $this->getLine(),
            'info' => $this->getMessage(),
            'time' => time(),
            'data' => $this->getRaw(),
        ];
    }

    /**
     * 将异常信息转换为字符串
     *
     * @return string 异常消息
     */
    public function toString()
    {
        return $this->getMessage();
    }
}