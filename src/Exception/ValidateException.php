<?php
declare(strict_types=1);

namespace Zlf\AppException\Exception;

use Zlf\AppException\ExceptionCode;

class ValidateException extends BasicsException
{
    /**
     * NotFoundException constructor.
     * @param string $message
     * @param null $raw
     */
    public function __construct(string $message = "数据验证失败", $raw = null)
    {
        parent::__construct($message, ExceptionCode::VALIDATION, $raw);
    }

}