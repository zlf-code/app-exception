<?php
declare(strict_types=1);

namespace Zlf\AppException\Exception;

use Zlf\AppException\ExceptionCode;

/**
 * 表单异常类
 */
class FormException extends BasicsException
{
    /**
     * NotFoundException constructor.
     * @param string $message
     * @param null $raw
     */
    public function __construct(string $message = "表单错误", $raw = null)
    {
        parent::__construct($message, ExceptionCode::ERROR, $raw);
    }

}